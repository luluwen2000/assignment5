#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]



@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    loaded_r = json.loads(r)
    return jsonify(loaded_r)
    
@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)

global_id = 4	

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    global global_id
    if request.method == 'POST':
        newBookName = request.form['name']
        books.append({"title" : newBookName, "id": str(global_id)})
        global_id += 1
        return showBook()
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        newName = request.form['name']
        for book in books:
            if book['id'] == str(book_id):
                book['title'] = newName
                break
        return render_template('showBook.html', books = books)
    else:
	    return render_template('editBook.html', book_id = book_id)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for i in range(len(books)):
            if books[i]['id'] == str(book_id):
                del books[i]
                break
        return render_template('showBook.html', books = books)
    else:
        return render_template('deleteBook.html', book = searchByID(str(book_id)))

def searchByName(book_name):
    for book in books:
        if book['title'] == book_name:
            return book
    return None

def searchByID(book_id):
    for book in books:
        if book['id'] == str(book_id):
            return book
    return None

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)